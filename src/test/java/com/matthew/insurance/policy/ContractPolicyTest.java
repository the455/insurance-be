package com.matthew.insurance.policy;

import com.matthew.insurance.io.ContractCreatedEvent;
import com.matthew.insurance.io.ContractTerminatedEvent;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class ContractPolicyTest {

    @Test
    public void testTerminationDateIsAfterStartDate() {
        final var contractCreatedEvent = new ContractCreatedEvent("Test", 500, LocalDate.now());
        final var contractTerminatedEvent = new ContractTerminatedEvent("Test", LocalDate.now().plusDays(5));

        assertTrue(ContractPolicy.isTerminationDateAfterStartDate(contractCreatedEvent, contractTerminatedEvent));
    }

    @Test
    public void testTerminationDateIsBeforeStartDate() {
        final var contractCreatedEvent = new ContractCreatedEvent("Test", 500, LocalDate.now());
        final var contractTerminatedEvent = new ContractTerminatedEvent("Test", LocalDate.now().minusDays(5));

        assertFalse(ContractPolicy.isTerminationDateAfterStartDate(contractCreatedEvent, contractTerminatedEvent));
    }

    @Test
    public void testTerminationDateIsTheSameAsStartDate() {
        final var contractCreatedEvent = new ContractCreatedEvent("Test", 500, LocalDate.now());
        final var contractTerminatedEvent = new ContractTerminatedEvent("Test", LocalDate.now());

        assertFalse(ContractPolicy.isTerminationDateAfterStartDate(contractCreatedEvent, contractTerminatedEvent));
    }

}