package com.matthew.insurance.resource;

import com.matthew.insurance.entity.Ledger;
import com.matthew.insurance.entity.repositories.LedgerRepository;
import com.matthew.insurance.io.*;
import com.matthew.insurance.policy.ContractPolicy;
import de.huxhorn.sulky.ulid.ULID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static com.matthew.insurance.io.DataEvents.CONTRACT_CREATED_EVENT;
import static com.matthew.insurance.io.DataEvents.CONTRACT_TERMINATED_EVENT;

@RestController
@RequestMapping("/contracts")
@CrossOrigin(origins = {
        "http://localhost:3000",
        "https://clever-poincare-f4af82.netlify.app"
})
public class ContractResource {

    private static final Logger log = LoggerFactory.getLogger(ContractResource.class);

    private final LedgerRepository ledgerRepository;
    private final ULID ulid;

    @Autowired
    public ContractResource(LedgerRepository ledgerRepository, ULID ulid) {
        this.ledgerRepository = ledgerRepository;
        this.ulid = ulid;
    }

    @PostMapping("create")
    @Transactional
    public void createContractEvent(@RequestBody ContractCreatedEvent event) {
        log.info("Attempt to create contract ({})", event.getContractId());
        final var contractCreatedEvent = ledgerRepository.findAllByContractIdAndEventName(
                event.getContractId(), CONTRACT_CREATED_EVENT.name()
        );

        if (contractCreatedEvent.isPresent()) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Contract already exists");
        }

        ledgerRepository.save(new Ledger(
                ulid.nextULID(),
                LocalDateTime.now(),
                CONTRACT_CREATED_EVENT.name(),
                event.getContractId(),
                event
        ));
        log.info("Contract ({}) created", event.getContractId());
    }

    @PostMapping("/terminate")
    public void terminateContractEvent(@RequestBody ContractTerminatedEvent event) {
        log.info("Attempt to terminate contract ({})", event.getContractId());
        final var contractCreatedEvent = ledgerRepository.findAllByContractIdAndEventName(
                event.getContractId(), CONTRACT_CREATED_EVENT.name()
        ).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No contract start found"));

        final var contractTerminatedEvent = ledgerRepository.findAllByContractIdAndEventName(
                event.getContractId(), CONTRACT_TERMINATED_EVENT.name()
        );

        if (contractTerminatedEvent.isPresent()) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Contract has already been terminated");
        }

        if (!ContractPolicy.isTerminationDateAfterStartDate(
                (ContractCreatedEvent) contractCreatedEvent.getPayload(), event
        )) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Termination date cannot be before start date");
        }

        ledgerRepository.save(new Ledger(
                ulid.nextULID(),
                LocalDateTime.now(),
                CONTRACT_TERMINATED_EVENT.name(),
                event.getContractId(), event
        ));
        log.info("Contract ({}) terminated", event.getContractId());
    }

    @GetMapping
    public List<ContractResultIOv1> getAllContractResults() {
        log.info("Getting contract information");
        var contractEventsMap = new HashMap<String, List<DataEvent>>();
        ledgerRepository.findAll()
                .forEach(ledger -> {
                    if (contractEventsMap.containsKey(ledger.getContractId())) {
                        var dataEvents = contractEventsMap.get(ledger.getContractId());
                        dataEvents.add(ledger.getPayload());
                        contractEventsMap.put(ledger.getContractId(), dataEvents);
                    } else {
                        var list = new ArrayList<DataEvent>();
                        list.add(ledger.getPayload());
                        contractEventsMap.put(ledger.getContractId(), list);
                    }
                });

        log.info("Found ({}) contracts", contractEventsMap.size());
        return contractEventsMap.values().stream()
                .map(dataEvents -> {
                    final var created = (ContractCreatedEvent) dataEvents.get(0);
                    if (dataEvents.size() > 1) {
                        final var terminated = (ContractTerminatedEvent) dataEvents.get(1);
                        return new ContractResultIOv1(
                                created.getContractId(),
                                created.getPremium(),
                                created.getStartDate(),
                                terminated.getTerminationDate()
                        );
                    } else {
                        return new ContractResultIOv1(
                                created.getContractId(),
                                created.getPremium(),
                                created.getStartDate(),
                                null
                        );
                    }
                })
                .sorted(Comparator.comparing(ContractResultIOv1::getStartDate))
                .collect(Collectors.toList());
    }
}
