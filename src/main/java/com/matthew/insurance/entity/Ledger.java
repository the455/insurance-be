package com.matthew.insurance.entity;

import com.matthew.insurance.io.DataEvent;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "ledger")
public class Ledger extends BaseEntity {

    private Long id;
    private String transactionId;
    private LocalDateTime createdAt;
    private String eventName;
    private String contractId;
    private DataEvent payload;

    public Ledger() {
    }

    public Ledger(String transactionId, LocalDateTime createdAt, String eventName, String contractId, DataEvent payload) {
        this.transactionId = transactionId;
        this.createdAt = createdAt;
        this.eventName = eventName;
        this.contractId = contractId;
        this.payload = payload;
    }

    @Id
    @Column(name = "id", nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "transaction_id", nullable = false)
    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Column(name = "created_at", nullable = false)
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Column(name = "contract_id", nullable = false)
    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    @Column(name = "event_name", nullable = false)
    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    @Type(type = "json")
    @Column(name = "payload", nullable = false)
    public DataEvent getPayload() {
        return payload;
    }

    public void setPayload(DataEvent payload) {
        this.payload = payload;
    }
}
