package com.matthew.insurance.entity.repositories;

import com.matthew.insurance.entity.Ledger;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface LedgerRepository extends CrudRepository<Ledger, Long> {

    Optional<Ledger> findAllByContractIdAndEventName(String contractId, String eventName);

    List<Ledger> findAll();
}
