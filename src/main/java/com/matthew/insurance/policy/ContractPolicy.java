package com.matthew.insurance.policy;

import com.matthew.insurance.io.ContractCreatedEvent;
import com.matthew.insurance.io.ContractTerminatedEvent;

public class ContractPolicy {

    public static boolean isTerminationDateAfterStartDate(
            ContractCreatedEvent contractCreatedEvent,
            ContractTerminatedEvent contractTerminatedEvent
    ) {
        return contractCreatedEvent.getStartDate().isBefore(contractTerminatedEvent.getTerminationDate());
    }
}
