package com.matthew.insurance.io;

public enum DataEvents {
    CONTRACT_CREATED_EVENT,
    CONTRACT_TERMINATED_EVENT
}
