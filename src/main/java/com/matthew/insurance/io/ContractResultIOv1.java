package com.matthew.insurance.io;

import java.time.LocalDate;

public record ContractResultIOv1(
        String contractId,
        int premium,
        LocalDate startDate,
        LocalDate terminationDate
) {

    public String getContractId() {
        return contractId;
    }

    public int getPremium() {
        return premium;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getTerminationDate() {
        return terminationDate;
    }
}
