package com.matthew.insurance.io;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;
import java.util.Objects;

public class ContractTerminatedEvent implements DataEvent {
    private String contractId;
    private LocalDate terminationDate;

    public ContractTerminatedEvent() {
    }

    public ContractTerminatedEvent(String contractId,
                                   LocalDate terminationDate) {
        this.contractId = contractId;
        this.terminationDate = terminationDate;
    }

    public String getContractId() {
        return contractId;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    public LocalDate getTerminationDate() {
        return terminationDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (ContractTerminatedEvent) obj;
        return Objects.equals(this.contractId, that.contractId) &&
                Objects.equals(this.terminationDate, that.terminationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contractId, terminationDate);
    }

    @Override
    public String toString() {
        return "ContractTerminatedEvent[" +
                "contractId=" + contractId + ", " +
                "terminationDate=" + terminationDate + ']';
    }

}