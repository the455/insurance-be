package com.matthew.insurance.io;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.time.LocalDate;

public class DataEventAdapter extends JsonDeserializer<DataEvent> {

    @Override
    public DataEvent deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        JsonNode node = p.getCodec().readTree(p);
        if (node.has("premium")) {
            return new ContractCreatedEvent(
                    node.get("contractId").asText(),
                    node.get("premium").asInt(),
                    toLocalDate(node.get("startDate").asText())
            );
        } else {
            return new ContractTerminatedEvent(
                    node.get("contractId").asText(),
                    toLocalDate(node.get("terminationDate").asText())
            );
        }
    }

    private LocalDate toLocalDate(String text) {
        return LocalDate.parse(text);
    }
}
