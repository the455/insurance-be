package com.matthew.insurance.io;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;
import java.util.Objects;

public class ContractCreatedEvent implements DataEvent {
    private String contractId;
    private int premium;
    private LocalDate startDate;

    public ContractCreatedEvent() {
    }

    public ContractCreatedEvent(String contractId,
                                int premium,
                                LocalDate startDate) {
        this.contractId = contractId;
        this.premium = premium;
        this.startDate = startDate;
    }

    public String getContractId() {
        return contractId;
    }

    public int getPremium() {
        return premium;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    public LocalDate getStartDate() {
        return startDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (ContractCreatedEvent) obj;
        return Objects.equals(this.contractId, that.contractId) &&
                this.premium == that.premium &&
                Objects.equals(this.startDate, that.startDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contractId, premium, startDate);
    }

    @Override
    public String toString() {
        return "ContractCreatedEvent[" +
                "contractId=" + contractId + ", " +
                "premium=" + premium + ", " +
                "startDate=" + startDate + ']';
    }

}