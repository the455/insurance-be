package com.matthew.insurance.io;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(using = DataEventAdapter.class)
public interface DataEvent {
}
