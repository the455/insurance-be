create table ledger
(
    id             int auto_increment,
    transaction_id varchar(26)             not null,
    contract_id    varchar(255)            not null,
    created_at     timestamp default NOW() not null,
    event_name     varchar(255)            not null,
    payload        JSON                    not null,
    constraint ledger
        primary key (id)
);

create index ledger_created_at_index
    on ledger (created_at);

create index ledger_contract_id_index
    on ledger (contract_id);

create index ledger_transaction_id__index
    on ledger (transaction_id);

