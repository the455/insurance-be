# Insurance backend

This is a spring boot Java 17 application, using flyway to manage the database migrations

To run you will need to configure the database setup in the `application.properties` file

On startup the application will run any migrations required.

Example endpoints

# Create contract

POST `/contracts/create`

```
{
    "contractId": "Matthew's Contract",
    "premium": 250,
    "startDate": "2021-11-13"
}
```

# Terminate contract

POST `/contracts/terminate`

```
{
    "contractId": "Matthew's Contract",
    "terminationDate": "2021-11-25"
}
```

# Get all contacts

GET `/contracts`

Returns all contracts with the termination day if available

# Improvements 

- The `DataEventAdapter` is not that scalable if we add additional events
- The `getAllContractResults` method is also very reliant on a specific order and casting, which may not be guaranteed over a long life service
- The `ContractResource` has quite a bit of logic in there, I prefer to have the controller or resource to do very little and pass the request to a handler service. But this seems a bit overkill for a small project